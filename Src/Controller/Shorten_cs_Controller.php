<?php
/**
 * @file
 * Contains \Drupal\book\Controller\BookController.
 */

namespace Drupal\book\Controller;

/**
 * Controller routines for shorten routes.
 */
class Shorten_cs_Controller {

  /**
   * Returns an administrative overview of all shorten URL's.
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function theme() {
    // ...
	
	 $form = \Drupal::formBuilder()->getForm('Drupal\shorten\Form\Shorten_cs_adminForm');
        /*return array(
        '#markup' => render($form),);*/
		return array(
        '#markup' => 'This is a shorten URLs test for shorten_cs.info.yml',);
		
  }
}

?>