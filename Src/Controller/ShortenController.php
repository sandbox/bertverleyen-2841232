<?php
/**
 * @file
 * Contains \Drupal\book\Controller\BookController.
 */

namespace Drupal\book\Controller;

/**
 * Controller routines for shorten routes.
 */
class ShortenController {

  /**
   * Returns an administrative overview of all shorten URL's.
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function AdminOverview() {
    // ...
	
	 $form = \Drupal::formBuilder()->getForm('Drupal\shorten\Form\ShortenKeysSettingsForm');
        return array(
        '#markup' => render($form),);
		/*return array(
        '#markup' => 'This is a shorten URLs test',);*/
		
  }
  
  public function shorten() {
     $form = \Drupal::formBuilder()->getForm('Drupal\shorten\Form\ShortenForm');
	 return array(
	 '#markup' => render($form),
	 );
  
  
  }
}

?>