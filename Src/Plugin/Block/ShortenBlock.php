<?php
namespace Drupal\shorten\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\block\BlockInterface;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides a 'Shorten URL's' Block
 *
 * @Block(
 * id = "shortenblock",
 * admin_label = @Translation("shorten url's block"),
 * )
 */
class ShortenBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build[] = array(
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '',
      '#attributes' => array(
        'id' => array('shorten URls'),
      ),
    );
	 $form = \Drupal::formBuilder()->getForm('Drupal\shorten\Form\Shorten_cs_AddForm');
                $build[] = array(
                "#markup" => render($form)
                );
	
    $build['#attached']['library'][] = 'shorten/drupal.shorten';

    return $build;
	  /*return array(
			'#markup' => $this->t('Hello, World!'),
	    );*/
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'id' => $this->getPluginId(),
      'label' => '',
      'provider' => $this->pluginDefinition['provider'],
      'label_display' => BlockInterface::BLOCK_LABEL_VISIBLE,
      'cache' => array(
        // Our blocks are not cacheable.
        'max_age' => 0,
      ),
    );

  }
}