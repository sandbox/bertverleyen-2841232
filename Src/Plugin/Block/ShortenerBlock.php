<?php
namespace Drupal\shorten\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\block\BlockInterface;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Provides a 'Shorten URL's' Block
 *
 * @Block(
 * id = "shortenerblock",
 * admin_label = @Translation("shortener url's block"),
 * )
 */
class ShortenerBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $build[] = array(
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => '',
      '#attributes' => array(
        'id' => array('Shortener'),
      ),
    );
      //$filter->settings += $defaults;
  //$build = array();
  $build['shortener_url_behavior'] = array(
    '#type' => 'radios',
    '#title' => t('Behavior'),
    '#default_value' => $this->config('shortener.settings')->get('shortener_url_behavior'],
    '#options' => array(
      'short' => t('Display the shortened URL by default, and add an "(expand)"/"(shorten)" link'),
      'strict' => t('Display the shortened URL by default, and do not allow expanding it'),
      'long' => t('Display the full URL by default, and add a "(shorten)"/"(expand)" link'),
    ),
  );
  $build['shortener_url_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum link text length'),
    '#default_value' => $this->config('shortener.settings')->get('shortener_url_length'),
    '#maxlength' => 4,
    '#description' => t('URLs longer than this number of characters will be truncated to prevent long strings that break formatting. The link itself will be retained; just the text portion of the link will be truncated.'),
  );
	
	/* $form = \Drupal::formBuilder()->getForm('Drupal\shorten\Form\Shorten_cs_AddForm');
                $build[] = array(
                "#markup" => render($form)
                );*/
	
    $build['#attached']['library'][] = 'shortener/drupal.shortener';

    return $build;
	  /*return array(
			'#markup' => $this->t('Hello, World!'),
	    );*/
  }
  
  
  
  

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'id' => $this->getPluginId(),
      'label' => '',
      'provider' => $this->pluginDefinition['provider'],
      'label_display' => BlockInterface::BLOCK_LABEL_VISIBLE,
      'cache' => array(
        // Our blocks are not cacheable.
        'max_age' => 0,
      ),
    );

  }
}