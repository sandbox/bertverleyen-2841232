<?php

namespace Drupal\shorten;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;



class Shorten_cs_DeleteForm extends ConfigFormBase{
   
   
    protected $service;
    function getFormId(){
        return 'delete_shorten__cs_delete';
    }
    
    protected function getEditableConfigNames(){
        return [
            'shorten.settings',
            ];
    }
	
	public function getQuestion(){
	  return t('Are you sure you want to delete the custom service %service?', array('%service' => $service->name));
	}
	
	public function getCancelUrl() {
      return new Url('shorten.myroute');
  }
	
    
   function buildForm(array $form, FormStateInterface $form_state, $service){
        
		//$config = $this->config('shorten.settings');
        $form['service'] = array(
    '#type' => 'value',
    '#value' => $service->name,
  );
  /*$form['#submit'] = array('shorten_cs_delete_submit');*/
  

		
       /* $form['shorten_overview'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('Delete button'),
        '#default_value' => $config->get('enable_overview_button'),
        );*/
        
        return parent::buildForm($form, $form_state);
    }
    
  function submitForm(array &$form, FormStateInterface $form_state){
        /*$this->config('shorten.settings')
            ->set('enable_overview_button', $form_state->getValue('shorten_overview'))
            ->save();*/
			
			$service = $form_state['values']['service'];
  if ($service == \Drupal::config('shorten_service')->get('is.gd')) {
    if (\Drupal::config('shorten_service_backup')->get('TinyURL') == 'is.gd') {
      \Drupal::config('shorten_service')->set('TinyURL');
    }
    else {
      \Drupal::config('shorten_service')->set('is.gd');
    }
    drupal_set_message(t('The default URL shortening service was deleted, so it has been reset to @service.', array('@service' => \Drupal::config('shorten_service')->get('is.gd'))));
  }
  if ($service == \Drupal::config('shorten_service_backup')->get('TinyURL')) {
    if (\Drupal::config('shorten_service')->get('is.gd') == 'TinyURL') {
      \Drupal::config('shorten_service_backup')->set('is.gd');
    }
    else {
      \Drupal::config('shorten_service_backup')->set('TinyURL');
    }
    drupal_set_message(t('The backup URL shortening service was deleted, so it has been reset to @service.', array('@service' => \Drupal::config('shorten_service_backup')->get('TinyURL'))));
  }
  db_delete('shorten_cs')
    ->condition('name', $service)
    ->execute();
  drupal_set_message(t('The service %service has been deleted.', array('%service' => $service)));
  $_GET['destination'] = 'admin/config/services/shorten/custom';
        
        parent::submitForm($form, $form_state);
    }
	
	
    
}