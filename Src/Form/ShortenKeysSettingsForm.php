<?php

namespace Drupal\shorten;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;



class ShortenKeysSettingsForm extends ConfigFormBase{
   
    function getFormId(){
        return 'shorten_urls_shorten_keys_settings';
    }
    
    protected function getEditableConfigNames(){
        return [
            'shorten.settings',
            ];
    }
    
    function buildForm(array $form, FormStateInterface $form_state){
        $config = $this->config('shorten.settings');
        $form['shorten_bitly'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bit.ly and j.mp'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['shorten_bitly']['shorten_bitly_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Bit.ly Login'),
    '#default_value' => $config->get('shorten_bitly_login'),
  );
  $form['shorten_bitly']['shorten_bitly_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Bit.ly API Key'),
    '#default_value' => $config->get('shorten_bitly_key'),
  );
  $form['shorten_budurl'] = array(
    '#type' => 'textfield',
    '#title' => t('BudURL API Key'),
    '#default_value' => $config->get('shorten_budurl'),
  );
  $form['shorten_cligs'] = array(
    '#type' => 'textfield',
    '#title' => t('Cligs API Key'),
    '#default_value' => $config->get('shorten_cligs'),
  );
  $form['shorten_ez'] = array(
    '#type' => 'textfield',
    '#title' => t('Ez API Key'),
    '#default_value' => $config->get('shorten_ez'),
  );
  $form['shorten_fwd4me'] = array(
    '#type' => 'textfield',
    '#title' => t('Fwd4.me API Key'),
    '#default_value' => $config->get('shorten_fwd4me'),
  );
  $form['shorten_googl'] = array(
    '#type' => 'textfield',
    '#title' => t('Goo.gl API Key'),
    '#default_value' => $config->get('shorten_googl'),
  );
  $form['shorten_redirec'] = array(
    '#type' => 'textfield',
    '#title' => t('Redir.ec API Key'),
    '#default_value' => \Drupal::config('shorten_redirec')->get(''),
  );
 
  
  /*$form['shorten_overview'] = array(
    '#type' => 'checkbox',
    '#title' => $this->t('Overview button'),
    '#default_value' => $config->get('enable_overview_button'),
  );*/
        
   return parent::buildForm($form, $form_state);
 }
 
  
    
    function submitForm(array &$form, FormStateInterface $form_state){
        $this->config('shorten.settings')
		      ->set('',$form_state->getValue('shorten_redirec'))
			  ->set('',$form_state->getValue('shorten_googl'))
			  ->set('',$form_state->getValue('shorten_fwd4me'))
			  ->set('',$form_state->getValue('shorten_ez'))
			  ->set('',$form_state->getValue('shorten_cligs'))
			  ->set('',$form_state->getValue('shorten_budurl'))
			  ->set('',$form_state->getValue('shorten_bitly')->getValue('shorten_bitly_login'))
			  ->set('',$form_state->getValue('shorten_bitly')->getValue('shorten_bitly_key'))
              //->set('enable_overview_button', $form_state->getValue('shorten_overview'))
              ->save();
	    
        
        parent::submitForm($form, $form_state);
    }
    
}