<?php

namespace Drupal\shorten;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;



class Shorten_cs_EditForm extends ConfigFormBase{
   
   
    protected $service;
    function getFormId(){
        return 'edit_shorten__cs_edit';
    }
    
    protected function getEditableConfigNames(){
        return [
            'shorten.settings',
            ];
    }
	
	
    
  function buildForm(array $form, FormStateInterface $form_state, $service){
   
         $form = array();
  foreach (array('name', 'url', 'type', 'tag') as $key) {
    $form[$key]['#default_value'] = $service->{$key};
  }
  $form['sid'] = array(
    '#type' => 'value',
    '#value' => $service->sid,
  );
  $form['old_name'] = array(
    '#type' => 'value',
    '#value' => $service->name,
  );
     return parent::buildForm($form, $form_state);
    }
	
  function validateForm($form, $form_state) {
  $v = $form_state['values'];
  if (($v['type'] == 'xml' || $v['type'] == 'json') && empty($v['tag'])) {
    $form_state->setErrorByName('type', t('An XML tag or JSON key is required for services with a response type of XML or JSON.'));
  }
  $exists = db_query("SELECT COUNT(sid) FROM {shorten_cs} WHERE name = :name AND sid <> :sid", array(':name' => $v['name'], ':sid' => $v['sid']))->fetchField();
  if ($exists > 0) {
    $form_state->setErrorByName('name', t('A service with that name already exists.'));
  }
  else {
    $all_services = \Drupal::moduleHandler()->invokeAll('shorten_service');
    $all_services['none'] = t('None');
    foreach ($all_services as $key => $value) {
      if ($key == $v['name']) {
        $form_state->setErrorByName('name', t('A service with that name already exists.'));
        break;
      }
    }
  }
}
	
    
  function submitForm(array &$form, FormStateInterface $form_state){
    
        $v = $form_state['values'];
  $record = array();
  foreach (array('name', 'url', 'type', 'tag', 'sid') as $key) {
    $record[$key] = $v[$key];
  }
  //drupal_write_record('shorten_cs', $record, 'sid');
  \Drupal::database()->merge('shorten_cs')
  ->key(array('url' => $v['url'], 'type' => $v['type'], 'name' => $v('name'), 'tag' => $v['tag'], 'sid' => $v['sid']))
  ->fields(array('url' => $v['url'], 'type' => $v['type'], 'name' => $v['name'], 'tag' => $v['tag'], 'sid' => $v['sid'])
  ->execute();
  
  \Drupal::keyValue('shorten_cs')->set('type',$v['type']);
  \Drupal::keyValue('shorten_cs')->set('name',$v['name']);
  \Drupal::keyValue('shorten_cs')->set('url',$v['url']);
  \Drupal::keyValue('shorten_cs')->set('tag',$v['tag']);
  \Drupal::keyValue('shorten_cs')->set('sid',$v['sid']);
  if ($v['old_name'] == \Drupal::config('shorten_service')->get('is.gd')) {
    \Drupal::config('shorten_service')->set($v['name']);
  }
  if ($v['old_name'] == \Drupal::config('shorten_service_backup')->get('TinyURL')) {
    \Drupal::config('shorten_service')->set($v['name']);
  }
  drupal_set_message(t('The changes to service %service have been saved.', array('%service' => $record['name'])));
  $_GET['destination'] = 'admin/config/services/shorten/custom';
        parent::submitForm($form, $form_state);
    }
	
	
    
}