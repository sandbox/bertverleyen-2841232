<?php

namespace Drupal\shorten\Shorten;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;


class Shorten_cs_AddForm implements FormInterface {
    function getFormID(){
        return 'add_shorten_cs_add';
    }
    
  function shorten_cs_services_table() {
  $header = array(t('Name'), t('URL'), t('Type'), t('XML/JSON tag'), t('Actions'));
  $rows = array();
  $result = db_query("SELECT * FROM {shorten_cs} ORDER BY name ASC")->fetchAll();
  foreach ($result as $service) {
    $service = (array) $service;
    $service = array(
      'sid' => $service['sid'],
      'name' => SafeMarkup::checkPlain($service['name']),
      'url' => SafeMarkup::checkPlain($service['url']),
      'type' => $service['type'],
      'tag' => SafeMarkup::checkPlain($service['tag']),
    );
    $service['actions'] = l(t('edit'), 'admin/config/services/shorten/custom/edit/' . $service['sid']) . ' ' .
      l(t('delete'), 'admin/config/services/shorten/custom/delete/' . $service['sid']);
    unset($service['sid']);
    $rows[] = $service;
  }
  if (!empty($rows)) {
    return theme('table', array('header' => $header, 'rows' => $rows));
  }
  return '';
}
	
	
    function buildForm(array $form, FormStateInterface $form_state){
        
		
		 $path = drupal_get_path('module','shorten_cs');
		 $path['#attached']['library'][] = 'js/shorten_cs.js';
  
		if (!isset($form) || !is_array($form)) {
				$form = array();
         }
		$form += array('#attributes' => array('class' => 'shorten-cs-apply-js'));
		$form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The name of the service'),
    '#required' => TRUE,
  );
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('API endpoint URL'),
    '#description' => t('The URL of the API endpoint, with parameters, such that the long URL can be appended to the end.') . ' ' .
      t('For example, the endpoint for TinyURL is') . ' <code>http://tinyurl.com/api-create.php?url=</code>. ' .
      t('Appending a long URL to the endpoint and then visiting that address will return data about the shortened URL.'),
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Response type'),
    '#description' => t('The type of response the API endpoint returns.'),
    '#required' => TRUE,
    '#default_value' => 'text',
    '#options' => array(
      'text' => t('Text'),
      'xml' => 'XML',
      'json' => 'JSON',
    ),
  );
  $form['tag'] = array(
    '#type' => 'textfield',
    '#title' => t('XML tag or JSON key'),
    '#description' => t('The XML tag or JSON key that identifies the full short URL in the API response.') . ' ' .
      t('Only required for XML and JSON response types.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
		
        return shorten_cs_services_table() . parent::buildForm($form, $form_state);
    }
    
  function validateForm(array &$form, FormStateInterface $form_state){
          $v = $form_state['values'];
  if (($v['type'] == 'xml' || $v['type'] == 'json') && empty($v['tag'])) {
    $form_state->setErrorByName('type', t('An XML tag or JSON key is required for services with a response type of XML or JSON.'));
  }
  $exists = db_query("SELECT COUNT(sid) FROM {shorten_cs} WHERE name = :name", array(':name' => $v['name']))->fetchField();
  if ($exists > 0) {
    $form_state->setErrorByName('name', t('A service with that name already exists.'));
  }
  else {
    $all_services = \Drupal::moduleHandler()->invokeAll('shorten_service');
    $all_services['none'] = t('None');
    foreach ($all_services as $key => $value) {
      if ($key == $v['name']) {
        $form_state->setErrorByName('name', t('A service with that name already exists.'));
        break;
      }
    }
  }
    }
    
    function submitForm(array &$form, FormStateInterface $form_state){
        $v = $form_state['values'];
  $record = array();
  foreach (array('name', 'url', 'type', 'tag') as $key) {
    $record[$key] = $v[$key];
  //}
  \Drupal::database()->merge('shorten_cs')
  ->key(array('url' => $v['url'], 'type' => $v['type'], 'name' => $v['name'], 'tag' => $v['tag']))
  ->fields(array('url' => $v['url'], 'type' => $v['type'], 'name' => $v['name'], 'tag' => $v['tag'])
  ->execute();
  //drupal_write_record('shorten_cs', $record);
  \Drupal::keyValue('shorten_cs')->set('type',$v['type']);
  \Drupal::keyValue('shorten_cs')->set('name',$v['name']);
  \Drupal::keyValue('shorten_cs')->set('url',$v['url']);
  \Drupal::keyValue('shorten_cs')->set('tag',$v['tag']);
   
   parent::submitForm($form, $form_state);
       
    }
}