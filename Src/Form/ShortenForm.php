<?php

namespace Drupal\shorten\Shorten;

use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;


class ShortenForm implements FormInterface {
   function getFormID(){
        return 'shorten_form';
    }
	
  function shorten_url($original = '', $service = '') {
  if (!$original) {
    $original = url($_GET['q'], array('absolute' => TRUE, 'alias' => !\Drupal::config('shorten_use_alias')->get(1)));
  }
  $original_for_caching = urlencode($original); // https://www.drupal.org/node/2324925
  if (!$service) {
    $service = \Drupal::config('shorten_service')->get('is.gd');
  }
  $cached = \Drupal::cache('cache_shorten')->get($original_for_caching);
  if (!empty($cached->data) && REQUEST_TIME < $cached->expire) {
    return $cached->data;
  }
  //$services = module_invoke_all('shorten_service');
  $services = Drupal::moduleHandler()->invokeAll('shorten_service');
  if (isset($services[$service])) {
    $url = _shorten_get_url($original, $services[$service], $service);
  }
  // If the primary service fails, try the secondary service.
  if (empty($url)) {
    $service = \Drupal::config('shorten_service_backup')->get('TinyURL');
    if (isset($services[$service])) {
      $url = _shorten_get_url($original, $services[$service], $service);
    }
    // If the secondary service fails, use the original URL.
    if (empty($url)) {
      $url = $original;
    }
  }
  $url = trim($url); // Redundant for most services.
  // Replace "http://" with "www." if the URL is abbreviated because it's shorter.
  if ($url != $original && \Drupal::config('shorten_www')->get(FALSE)) {
    if (strpos($url, 'http://') === 0) {
      $url = drupal_substr($url, 7);
      if (strpos($url, 'www.') !== 0) {
        $url = 'www.' . $url;
      }
    }
    elseif (strpos($url, 'https://') === 0) {
      $url = drupal_substr($url, 8);
      if (strpos($url, 'www.') !== 0) {
        $url = 'www.' . $url;
      }
    }
  }
  $cache_duration = \Drupal::config('shorten_cache_duration')->get(1814400);
  // Only cache failed retrievals for a limited amount of time.
  if ($url == $original) {
    $expire = REQUEST_TIME + \Drupal::config('shorten_cache_fail_duration')->get(1800);
  }
  elseif (is_numeric($cache_duration)) {
    $expire = REQUEST_TIME + $cache_duration;
  }
  else {
    $expire = CACHE_PERMANENT;
  }
  cache_set($original_for_caching, $url, 'cache_shorten', $expire);
  //module_invoke_all('shorten_create', $original, $url, $service);
  Drupal::moduleHandler()->invokeAll('shorten_create', $original, $url, $service);
  return $url;
}

function _shorten_get_url($original, $api, $service) {
  $method = Unicode::strtoupper(\Drupal::config('shorten_method')->get(_shorten_method_default()));
  $service = t('an unknown service');
  if (is_string($api)) {
    $url = shorten_fetch($api . urlencode($original));
    $service = $api;
  }
  elseif (is_array($api)) {
    // Merge in defaults.
    $api += array(
      'custom' => FALSE,
      'json' => FALSE,
    );
    if (!empty($api['url'])) {
      $original = urlencode($original);
      // Typically $api['custom'] == 'xml' although it doesn't have to.
      if (!empty($api['tag'])) {
        $url = shorten_fetch($api['url'] . $original, $api['tag']);
      }
      elseif (!empty($api['json'])) {
        $url = shorten_fetch($api['url'] . $original, $api['json'], 'json');
      }
      elseif (!$api['custom']) {
        $url = shorten_fetch($api['url'] . $original);
      }
      $service = $api['url'];
    }
    elseif (is_string($api['custom']) && function_exists($api['custom'])) {
      $method =  t('A custom method: @method()', array('@method' => $api['custom']));
      if (!empty($api['args']) && is_array($api['args'])) {
        $args = $api['args'];
        array_unshift($args, $original);
        $url = call_user_func_array($api['custom'], $args);
      }
      else {
        $url = call_user_func($api['custom'], $original);
      }
    }
  }

  if ($url) {
    if (drupal_substr($url, 0, 7) == 'http://' || drupal_substr($url, 0, 8) == 'https://') {
      return $url;
    }
  }
  //watchdog('shorten', '%method failed to return an abbreviated URL from %service.', array('%method' => $method, '%service' => $service), WATCHDOG_NOTICE, $url);
  \Drupal::logger('shorten')->notice('%method failed to return an abbreviated URL from %service.');
  return FALSE;
}

	
	
    
 function buildForm(array $form, FormStateInterface $form_state){
         //drupal_add_js(drupal_get_path('module', 'shorten') . '/shorten.js');
  $path = drupal_get_path('module','shorten');
  $path['#attached']['library'] = 'js/shorten.js';
  //Form elements between ['opendiv'] and ['closediv'] will be refreshed via AHAH on form submission.
  $form['opendiv'] = array(
    '#markup' => '<div id="shorten_replace">',
  );
  if (!isset($form_state['storage'])) {
    $form_state['storage'] = array('step' => 0);
  }
  if (isset($form_state['storage']['short_url'])) {
    // This whole "step" business keeps the form element from being cached.
    $form['shortened_url_' . $form_state['storage']['step']] = array(
      '#type' => 'textfield',
      '#title' => t('Shortened URL'),
      '#default_value' => $form_state['storage']['short_url'],
      '#size' => 25,
      '#attributes' => array('class' => array('shorten-shortened-url')),
    );
  }
  $form['url_' . $form_state['storage']['step']] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => '',
    '#required' => TRUE,
    '#size' => 25,
    '#maxlength' => 2048,
    '#attributes' => array('class' => array('shorten-long-url')),
  );
  //Form elements between ['opendiv'] and ['closediv'] will be refreshed via AHAH on form submission.
  $form['closediv'] = array(
    '#markup' => '</div>',
  );
  $last_service = NULL;
  if (isset($form_state['storage']['service'])) {
    $last_service = $form_state['storage']['service'];
  }
  $service = _shorten_service_form($last_service);
  if (is_array($service)) {
    $form['service'] = $service;
  }
  $form['shorten'] = array(
    '#type' => 'submit',
    '#value' => t('Shorten'),
    '#ajax' => array(
      'callback' => 'shorten_save_js',
      'wrapper' => 'shorten_replace',
      'effect' => 'fade',
      'method' => 'replace',
    ),
  );
    return parent::buildForm($form, $form_state);
 }
    
  function validateForm(array &$form, FormStateInterface $form_state){
  
  $url = $form_state['values']['url_' . $form_state['storage']['step']];
  if (drupal_strlen($url) > 4) {
    if (!strpos($url, '.', 1)) {
      $form_state->setErrorByName('url', t('Please enter a valid URL.'));
    }
  }
  else {
    $form_state->setErrorByName('url', t('Please enter a valid URL.'));
  }
  
 }
    
 function submitForm(array &$form, FormStateInterface $form_state){
  $service = '';
  if ($form_state['values']['service']) {
    $service = $form_state['values']['service'];
  }
  $shortened = shorten_url($form_state['values']['url_' . $form_state['storage']['step']], $service);
  if ($form_state['values']['service']) {
    $_SESSION['shorten_service'] = $form_state['values']['service'];
  }
  drupal_set_message(t('%original was shortened to %shortened', array('%original' => $form_state['values']['url_' . $form_state['storage']['step']], '%shortened' => $shortened)));
  $form_state['rebuild'] = TRUE;
  if (empty($form_state['storage'])) {
    $form_state['storage'] = array();
  }
  $form_state['storage']['short_url'] = $shortened;
  $form_state['storage']['service']   = $form_state['values']['service'];
  if (isset($form_state['storage']['step'])) {
    $form_state['storage']['step']++;
  }
  else {
    $form_state['storage']['step'] = 0;
  }
   
   parent::submitForm($form, $form_state);
       
 }
}